# Summer-Internship-I



## Introduction

This document outlines my experiences and contributions during my summer internship at Telecom Gabes.
 As my initial foray into the IT field, this document delves into my discoveries and details the tasks I undertook throughout this internship

## Access technologies used

To ensure data transmission activities, there are several transmission technologies that
do not have the same flow capacity. Among these technologies we can cite
⦁ ADSL & SDSL
⦁ MPLS
⦁ Frame Relay

## ADSL & SDSL
The term ADSL stands for Asymmetric Digital Subscriber Line and is a digital communication technique (physical layer) of the xDSL family. It allows you to use a telephone line, a specialized line, or even an ISDN line (in English ISDN, or integrated services digital network), to transmit and receive digital data independently of the conventional telephone service (i.e. i.e. analog) via an ADSL filter connected to the socket. As such, this method of communication differs from that used when operating so-called "analog" modems, whose signals are exchanged within the framework of telephone communication (similar to fax, that is to say on voice frequencies). ADSL technology is widely implemented by Internet service providers to support so-called "high-speed" access.

![Local Image](images/ADSL.png)

ADSL offers much better speeds than Minitel, or Internet via PSTN modem. However, when a connection problem occurs, it is usually difficult to resolve.
For ADSL access, the subscriber must follow the following steps, as shown in the diagram below.

![Local Image](images/SDSL.jpg)

## Frame Relay 
Frame Relay is a packet-switched protocol located at the link layer (level 2) of the OSI model, used for inter-site exchanges (WAN). frame relay is a technology that makes it possible to replace leased connections (expensive because they are dedicated to a single customer) with a frame relay "cloud".
shared between numerous clients. The access provider, assuming that there is little chance that all of its customers will need maximum bandwidth simultaneously, offers its customers a contract indicating an Excess Information rate (or burst), i.e. -say the maximum flow rate accepted on the
Frame Relay network and a CIR (Committed Information Rate), i.e. a minimum guaranteed throughput.

![Local Image](images/Frame-Relay.jpg)

## MPLS
In computer networks and telecommunications, MultiProtocol Label Switching (MPLS) is a data transport mechanism based on the switching of labels or "labels", which are inserted at the entrance to the MPLS network and removed at its exit a l Originally, this insertion takes place between the data link layer (level 2) and the network layer (level 3) in order to transport protocols like IP. This is why MPLS is referred to as a "2.5" layer protocol. This protocol has evolved to provide a unified data transport service for clients using a packet switching technique. MPLS can be used to carry virtually any type of traffic, for example voice or IPv4, IPv6 packets and even Ethernet or ATM frames.
MPLS makes it possible to route different types of traffic on a single infrastructure while respecting the associated operating constraints.

![Local Image](images/MPLS.png)



## General Conclusion 
During the internship month that I carried out at Tunisie Telecom, I was impressed by the multitude of its activities. Indeed, this experience allowed me to learn to perform various tasks and to gain an understanding of the equipment of the Business Unit as well as its mode of operation.
Finally, this internship allowed me to deepen my knowledge in the field of telecommunications, and it remains an excellent opportunity to get a closer look at professional life.




